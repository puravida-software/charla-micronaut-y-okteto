package bigcollatz

import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class CollatzControllerSpec extends Specification{

    @Shared
    PostgreSQLContainer container = new PostgreSQLContainer()

    @Inject
    @Client("/")
    RxHttpClient rxHttpClient

    @Inject
    CollatzRepository repository

    void test(){
        given:"a really big number"
        BigInteger bi = new BigInteger("10")

        when:"request the counter"
        int count = rxHttpClient.toBlocking().retrieve("/${bi}", int)

        then:"the counter is ok"
        count == 6

        and: "the collatz is save"
        repository.count() == 1
    }

}
