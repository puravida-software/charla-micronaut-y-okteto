package bigcollatz

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/")
class CollatzController {

    CollatzService collatzService

    CollatzController(CollatzService collatzService) {
        this.collatzService = collatzService
    }

    @Get("/{n}")
    int count(BigInteger n){
        collatzService.count(n)
    }

}
