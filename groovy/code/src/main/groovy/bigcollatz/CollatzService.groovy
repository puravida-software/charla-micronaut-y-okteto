package bigcollatz

import javax.inject.Singleton

@Singleton
class CollatzService {

    CollatzRepository repository

    CollatzService(CollatzRepository repository) {
        this.repository = repository
    }

    int count(BigInteger n){
        String id = n.toString()
        Optional<CollatzEntity> opt = repository.findById(id)
        if( opt.isPresent())
            return opt.get().counter

        int count = 0
        while (!n.equals(BigInteger.ONE)) {
            if (n.mod(new BigInteger("2")) == BigInteger.ZERO) {
                n = n.divide(new BigInteger("2"))
            } else {
                n = n.multiply(new BigInteger("3"))
                n = n.add(new BigInteger("1"))
            }
            count++
        }

        CollatzEntity entity = new CollatzEntity()
        entity.id = id
        entity.counter = count
        repository.save(entity)
        count
    }

}
