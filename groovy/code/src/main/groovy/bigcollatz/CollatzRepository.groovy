package bigcollatz

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository

import javax.transaction.Transactional

@JdbcRepository(dialect = Dialect.POSTGRES)
interface CollatzRepository extends CrudRepository<CollatzEntity, String>{

    long count()

    @Transactional
    CollatzEntity save(CollatzEntity entity)

    Optional<CollatzEntity>findById(String id)

}
