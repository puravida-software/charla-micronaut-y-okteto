package bigcollatz

import io.micronaut.context.annotation.Provided
import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

import javax.persistence.Table

@MappedEntity
@Table(name="collatz")
class CollatzEntity {

    @Id
    @Provided
    String id

    int counter


}
