package bigcollatz

import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class CollatzServiceTest extends Specification {

    @Inject
    CollatzService collatzService

    def "Compute"() {
        when: "compute 10"
        int count = collatzService.compute(new BigInteger("10"))
        then:
        count == 6
    }
}
