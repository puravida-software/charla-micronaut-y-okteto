package bigcollatz

import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class CollatzControllerTest extends Specification {

    @Inject
    @Client("/")
    RxHttpClient rxHttpClient

    @Inject
    CollatzRepository repository

    def "Compute"() {
        when: "request a number"
        int count = rxHttpClient.toBlocking().retrieve("/10",int)
        then:
        count == 6
        and:
        repository.count()==1
    }
}
