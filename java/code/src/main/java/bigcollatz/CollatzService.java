package bigcollatz;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigInteger;
import java.util.Optional;

@Singleton
public class CollatzService {

    @Inject
    CollatzRepository repository;

    public int compute(BigInteger n){
        String id = n.toString();
        Optional<CollatzEntity> opt = repository.findById(id);
        if( opt.isPresent() )
            return opt.get().counter;

        int count = 0;
        while (!n.equals(BigInteger.ONE)) {
            if (n.mod(new BigInteger("2")) == BigInteger.ZERO) {
                n = n.divide(new BigInteger("2"));
            } else {
                n = n.multiply(new BigInteger("3"));
                n = n.add(new BigInteger("1"));
            }
            count++;
        }

        CollatzEntity entity = new CollatzEntity(id, count);
        repository.save(entity);

        return count;
    }

}
