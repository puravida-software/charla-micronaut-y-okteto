package bigcollatz;

import io.micronaut.context.annotation.Provided;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;

import javax.persistence.Table;

@MappedEntity
@Table(name="collatz")
public class CollatzEntity {

    @Id
    @Provided
    String id;

    int counter;

    public CollatzEntity(String id, int counter) {
        this.id = id;
        this.counter = counter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
