package bigcollatz;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface CollatzRepository extends CrudRepository<CollatzEntity, String> {

    public long count();

    @Transactional
    public CollatzEntity save(CollatzEntity entity);

    public Optional<CollatzEntity> findById(String id);

}
