package bigcollatz;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import javax.inject.Inject;
import java.math.BigInteger;

@Controller("/")
public class CollatzController {

    @Inject
    CollatzService collatzService;

    @Get("{n}")
    int compute(BigInteger n){
        return collatzService.compute(n);
    }

}
